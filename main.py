import json
from functools import partial
import pudb
import os
import urllib

import tornado.ioloop
import tornado.auth
import tornado.web
import tornado.httpserver
import tornado.httpclient
import tornado.autoreload
import tornado.options

import conf

settings = {
    'debug': True,
    'cookie_secret': conf.cookie_secret,
    'secure_cookie_expire_days': None,#1.0/60/60/24 * 5,
    'login_url': '/login',
}

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.cli = tornado.httpclient.AsyncHTTPClient()
        self.mongo_api_key = conf.mongo_api_key

    def get_current_user(self):
        user = self.get_secure_cookie('user')
        if user:
            self.set_secure_cookie('user', user,
                expires_days=self.settings['secure_cookie_expire_days'])
        return user

    def logout_current_user(self):
        self.clear_cookie('user')

class GoogleLoginHandler(BaseHandler, tornado.auth.GoogleMixin):
    @tornado.web.asynchronous
    def get(self):
        code = self.get_argument("code", None)
        self.next = '/user'
        if code:
            url = conf.google_token_url
            body = ("grant_type=authorization_code&"
                "redirect_uri={redirect_uri}&"
                "client_secret={client_secret}&"
                "client_id={client_id}&"
                "code={code}".format(
                    redirect_uri=conf.google_auth_redirect_uri,
                    client_secret=conf.google_auth_client_secret,
                    client_id=conf.google_auth_client_id,
                    code = code))
            req = tornado.httpclient.HTTPRequest(url, method='POST', body=body)
            self.cli.fetch(req, self._on_token)
        else:
            url = ("{url}?"
                "scope={scope}&"
                "state={next}&"
                "redirect_uri={redirect_uri}&"
                "response_type=code&"
                "access_type=offline&"
                "client_id={client_id}".format(
                    url=conf.google_auth_url,
                    scope=conf.google_auth_scope,
                    redirect_uri=conf.google_auth_redirect_uri,
                    client_id=conf.google_auth_client_id,
                    next=self.next))
            self.redirect(url)

    def _on_token(self, response):
        response = json.loads(response.body)
        self.access_token = response['access_token']
        req = tornado.httpclient.HTTPRequest(
            conf.google_auth_user_info_url,
            method='GET',
            headers={'Authorization': 'Bearer {}'.format(
                self.access_token)})
        self.cli.fetch(req, self._on_user)

    def _on_user(self, response):
        response = json.loads(response.body)
        response['access_token'] = self.access_token
        self.set_secure_cookie('user', json.dumps(response),
            expires_days=self.settings['secure_cookie_expire_days'])
        self.redirect(self.next)

class UserLogoutHandler(BaseHandler):
    def get(self):
        self.logout_current_user()
        self.redirect('/')

class MainHandler(BaseHandler):
    def get(self):
        user = self.get_current_user()
        if user:
            self.redirect('/user')
        else:
            with open('index.html') as f:
                html = f.read()
            self.write(html)

class UserHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        with open('user.html') as f:
            html = f.read()
        self.write(html)

class AuthHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.redirect('/')

class QueryHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, query):
        if query:
            if self.request.host in conf.mogno_queries_authorized_hosts:
                user = json.loads(self.get_current_user())
                cli = tornado.httpclient.AsyncHTTPClient()
                url = '{host}/{path}/?key={key}&user={user}'.format(
                    host=conf.mongo_api_host.strip('/'),
                    path=urllib.quote(query.encode('utf8')),
                    key=self.mongo_api_key,
                    user=user['email']
                )
                cb = partial(self._on_response, query=query)
                cli.fetch(url, cb)
            else:
                self.finish({
                    'status': -1,
                    'data': '',
                    'msg': 'Unauthorized host'
                })

    def _on_response(self, response, query=None):
        response = not isinstance(response.body, basestring) and response.body or json.loads(response.body)
        if isinstance(response, dict) and 'status' in response:
            if response['status'] == 0:
                self.finish({
                    'status': 0,
                    'data': response['data'],
                    'msg': u'Query successfully executed: "{}". Response: {}'.format(query, response['data'])
                })
            else:
                self.finish({
                    'status': -1,
                    'data': None,
                    'msg': response['msg']
                })
        else:
            self.finish({
                'status': 0,
                'data': response,
                'msg': u'Query successfully executed: "{}". Response: {}'.format(query, response)
            })

class CurrentUserHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        user = self.get_current_user()
        if user is None:
            result = {
                'status': -1,
                'data': '',
                'msg': 'Unauthorized'
            }
        else:
            result = {
                'status': 0,
                'data': json.loads(user),
                'msg': ''
            }
        self.write(result)

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/user/?", UserHandler),
    (r"/auth/?", AuthHandler),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {
        "path": os.path.join(os.path.dirname(__file__), "static")
    }),
    (r"/query/(.*)", QueryHandler),
    (r"/login/?", GoogleLoginHandler),
    (r"/current-user/?", CurrentUserHandler),
    (r"/logout/?", UserLogoutHandler)
], **settings)
tornado.options.define("port", default=9999, help="run on the given port", type=int)
tornado.options.parse_command_line()
http_server = tornado.httpserver.HTTPServer(application)
http_server.listen(tornado.options.options.port)
tornado.autoreload.start()
tornado.ioloop.IOLoop.instance().start()
