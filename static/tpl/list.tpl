<table class="table-hover table table-condensed" data-ng-hide="alerts.loading">
  <thead>
    <tr class="success">
      <td><input id="focus_input" type="text" data-ng-model="new_source" class="form-control" placeholder="New source" data-ng-keypress="create_record_event($event)"></td>
      <th style="width: 15px;"></th>
      <td><input type="text" data-ng-model="new_translation" class="form-control" placeholder="New translation" data-ng-keypress="create_record_event($event)"></td>
      <td style="width: 85px"><a data-ng-href="" class="btn btn-default btn-icon" data-ng-click="create_record()"><i class="icon-list-add"></i></a> <i class="icon-search"></i></td>
    </tr>
    <tr>
      <th><a data-ng-href="" class="btn btn-default btn-icon" style="padding:0" data-ng-click="sort_by = 'source';"><i class="icon-sort-name-up"></i></a> 
        <span data-ng-switch="show_source">
          <a data-ng-href="" data-ng-switch-when="false" class="btn btn-default btn-icon" data-ng-click="toggle_view('source')"><i class="icon-eye-off"></i></a>
          <a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="toggle_view('source')"><i class="icon-eye"></i></a>
        </span>
      </th>
      <th style="width: 15px;"></th>
      <th><a data-ng-href="" class="btn btn-default btn-icon" style="padding:0" data-ng-click="sort_by = 'translation';"><i class="icon-sort-name-up"></i></a>
        <span data-ng-switch="show_translation">
          <a data-ng-href="" data-ng-switch-when="false" class="btn btn-default btn-icon" data-ng-click="toggle_view('translation')"><i class="icon-eye-off"></i></a>
          <a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="toggle_view('translation')"><i class="icon-eye"></i></a>
        </span>
      </th>
      <th><a data-ng-href="" class="btn btn-default btn-icon" style="padding:0" data-ng-click="sort_by = 'added_time';"><i class="icon-down"></i><i class="icon-clock"></i></a><a data-ng-href="" class="btn btn-default btn-icon" data-ng-click="show_archived = !show_archived"><i class="icon-archive"></i><i data-ng-hide="show_archived" class="icon-eye-off"></i><i data-ng-show="show_archived" class="icon-eye"></i></a>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr data-ng-repeat="record in records | filter:new_source | filter:new_translation | orderBy:sort_by:true" data-ng-hide="record.archived && !show_archived">
      <td data-ng-switch="record.edit">
        <input type="text" class="form-control" data-ng-switch-when="true" data-ng-model="record.update_source" data-ng-keypress="update_record_event($event, record)">
        <div data-ng-switch-default data-ng-show="show_source" title="{{record.translation}}">
          {{record.source}}
        </div>
      </td>
      <td style="width: 15px;">
        <div data-ng-show="record.edit == true">
          <a data-ng-href="" class="btn btn-default btn-icon" data-ng-click="switch(record)"><i class="icon-exchange"></i></a>
        </div>
      </td>
      <td data-ng-switch="record.edit">
        <input type="text" class="form-control" data-ng-switch-when="true" data-ng-model="record.update_translation" data-ng-keypress="update_record_event($event, record)">
        <div data-ng-switch-default data-ng-show="show_translation" title="{{record.source}}">
          {{record.translation}}
        </div>
      </td>
      <td>
        <div data-ng-switch="record.edit">
          <a data-ng-href="" data-ng-switch-when="true" class="btn btn-default btn-icon" data-ng-click="update_record(record)"><i class="icon-check"></i></a>
          <a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="record.edit = true"><i class="icon-pencil"></i></a>
          <a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="delete_record(record)"><i class="icon-trash"></i></a>
          <a data-ng-href="" data-ng-switch-default data-ng-show="record.archived" class="btn btn-warning btn-icon" data-ng-click="dearchive_record(record)"><i class="icon-archive"></i></a>
          <a data-ng-href="" data-ng-switch-default data-ng-hide="record.archived" class="btn btn-default btn-icon" data-ng-click="archive_record(record)"><i class="icon-archive"></i></a>
        </div>
      </td>
    </tr>
  </tbody>
</table>