DEBUG_MODE_ON = true;
if (!DEBUG_MODE_ON) {
    console = console || {};
    console.debug = function(){};
}

angular.module('vocabs', []).
  config(function($routeProvider) {
    $routeProvider.
      when('/list/:listName', {controller:ListCtrl, templateUrl:'static/tpl/list.tpl'}).
      otherwise({redirectTo:'/'});
  }).
  directive('ngConfirmClick', [
    function(){
      return {
        priority: 100,
        restrict: 'A',
        link: function(scope, element, attrs){
          element.bind('click', function(e){
            var message = attrs.ngConfirmClick;
            if(message && !confirm(message)){
              e.stopImmediatePropagation();
              e.preventDefault();
            }
          });
        }
      }
    }
  ]);

function MainCtrl($scope, $http) {
  $scope.isCollapsed = false;
  $scope.user = undefined;
  $scope.current_collection = '';
  $scope.new_vocabulary_name = '';
  $scope.alerts = new Object;
  $scope.alerts.loading = false;
  $http.get('/current-user').success(function(data){
    if (data.status == 0){
      $scope.user = data.data;
      console.debug(data.msg);
      $http.get('/query/get-collections').success(function(data){
        if(data.status == 0){
          $scope.collections = data.data;
          console.debug(data.msg);
        } else {
          $scope.collections = [];
          console.error(data.msg);
        }
      });
    } else {
      $scope.user = undefined;
      console.error(data.msg);
    }
  });

  $scope.create_collection = function(){
    if($scope.new_vocabulary_name != '' && !/[#&?/]/.test($scope.new_vocabulary_name)){
      var new_collection_name_holder = $scope.new_vocabulary_name;
      $scope.collections.push($scope.new_vocabulary_name);
      $scope.new_vocabulary_name = '';
      $http.get('/query/create-collection/' + new_collection_name_holder).success(function(data){
        if(data.status == 0){
          console.debug(data.msg);
        } else {
          console.error(data.msg);
          window.location.reload();
        }
      });
    }
  }
  $scope.create_collection_event = function($event){
    if ($event.keyCode == 13) {
      $scope.create_collection();
    }
  }

  $scope.set_current_collection = function(collection){
    $scope.current_collection = collection;
    if($scope.current_collection != collection){
      $scope.alerts.loading = true;
    }
  }

  $scope.delete_collection = function(collection_name){
    var index = $scope.collections.indexOf(collection_name);
    $scope.collections.splice(index, 1);
    $http.get('/query/delete-collection/' + collection_name).success(function(data){
      if(data.status == 0){
        console.debug(data.msg);
        if($scope.current_collection == collection_name) {
          window.location.href = '#';
        }
      } else {
        console.error(data.msg);
        window.location.reload();
      }
    });
  }
  $scope.logout = function(){
    $scope.user = undefined;
    window.location.href = '/logout';
  }
}

function ListCtrl($scope, $http, $routeParams) {
  $scope.show_archived = false;
  $scope.sort_by = 'added_time';
  $scope.new_scource = '';
  $scope.new_translation = '';
  $scope.show_source = true;
  $scope.show_translation = true;
  $scope.list_name = $routeParams.listName;
  $scope.records = null;
  $http.get('/query/get/'+ $routeParams.listName).success(function(data){
    if(data.status == 0){
      for(var i = 0; i < data.data.length; i ++){
        data.data[i].update_source = data.data[i].source;
        data.data[i].update_translation = data.data[i].translation;
      }
      $scope.records = data.data;
      console.debug(data.msg);
      $scope.alerts.loading = false;
    } else {
      $scope.records = [];
      console.error(data.msg);
    }
  });
  $scope.create_record = function(){
    if(!/[#&?/]/.test($scope.new_source) && !/[#&?/]/.test($scope.new_translation)) {
      if($scope.new_source != '' && $scope.new_translation != ''){
        var i = $scope.records.push({
          'source': $scope.new_source,
          'translation': $scope.new_translation,
          'update_source': $scope.new_source,
          'update_translation': $scope.new_translation,
          'added_time': new Date().getTime(),
          'archived': false,
          '_id': '' //TBD
        });
        i -= 1;
        $http.get('/query/insert/' + $scope.list_name + '/' + $scope.new_source + '/' + $scope.new_translation).success(function(data){
          if(data.status == 0){
            $scope.records[i]._id = data.data._id;
            $scope.new_source = '';
            $scope.new_translation = '';
            document.getElementById("focus_input").focus();
            console.debug(data.msg);
          } else {
            console.error(msg);
          }
        });
      }
    }
  }
  $scope.create_record_event = function($event){
    if($event.keyCode == 13){
      $scope.create_record();
    }
  }
  $scope.archive_record = function(record){
    record.archived = true;
    $http.get('/query/archive/' + $scope.list_name + '/' + record._id.$oid).success(function(data){
      if(data.status == 0){
        console.debug(data.msg);
      } else {
        console.error(data.msg);
        window.location.reload();
      }
    });
  }
  $scope.dearchive_record = function(record){
    record.archived = false;
    $http.get('/query/dearchive/' + $scope.list_name + '/' + record._id.$oid).success(function(data){
      if(data.status == 0){
        console.debug(data.msg);
      } else {
        console.error(data.msg);
        window.location.reload();
      }
    });
  }
  $scope.delete_record = function(record){
    var index = $scope.records.indexOf(record);
    $scope.records.splice(index, 1);
    $http.get('/query/delete/' + $scope.list_name + '/' + record._id.$oid).success(function(data){
      if(data.status == 0){
        console.debug(data.msg);
      } else {
        console.error(data.msg);
        window.location.reload();
      }
    });
  }
  $scope.update_record = function(record){
    var index = $scope.records.indexOf(record);
    $scope.records[index].source = $scope.records[index].update_source;
    $scope.records[index].translation = $scope.records[index].update_translation;
    $scope.records[index].edit = false;
    $http.get('/query/update/' + $scope.list_name + '/' + record._id.$oid + '/' + record.update_source + '/' + record.update_translation).success(function(data){
      if(data.status == 0){
        console.debug(data.msg);
      } else {
        console.error(data.msg);
        window.location.reload();
      }
    });
  }
  $scope.update_record_event = function($event, record){
    if($event.keyCode == 13){
      $scope.update_record(record);
    }
  }
  $scope.switch = function(record){
    var tmp = record.update_source;
    record.update_source = record.update_translation;
    record.update_translation = tmp;
  }
  $scope.toggle_view = function(view){
    if(view == 'translation'){
      $scope.show_translation = $scope.show_translation === true ? false : true;
    } else if(view == 'source'){
      $scope.show_source = $scope.show_source === true ? false : true;
    }
  }
}